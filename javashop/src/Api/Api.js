import Network from './Http';
import uuidv1 from "uuid/v1"
// 封装各种接口请求
// export const 接口名 = () => Network.get('/路由',参数对象);
const jsp = {
    //轮播图
    getSwiper: () => Network.get('/buyer/focus-pictures', {client_type: 'WAP'}),
    //轮播图下方小分类
    getLittletype: () => Network.get('/buyer/pages/site-navigations', {client_type: 'MOBILE'}),
    //轮播图上方分类
    getToptype: () => Network.get('/buyer/goods/categories/0/children', {}),
    //分类搜索需要getToptype的店铺id
    getSearchtype: (a, b) => Network.get('/buyer/goods/search', {page_no: b, page_size: 10, category: a}),
    //首页的数据
    getHomedata: () => Network.get('/buyer/pages/WAP/INDEX', {}),
    //点击搜索时默认推荐搜索
    recommended: () => Network.get('/buyer/pages/hot-keywords'),
    //关键字模糊搜索
    searchFuzzy: (a) => Network.get('/buyer/goods/search/words', {keyword: a}),
    //关键字搜索功能
    searchMethod: (a, b) => Network.get('/buyer/goods/search', {page_no: b, page_size: 10, keyword: a}),
    //搜索的销量
    searchSale: (a, b) => Network.get('/buyer/goods/search', {
        page_no: b,
        page_size: 10,
        keyword: a,
        sort: 'buynum_desc'
    }),
    //店铺搜索
    searchShop: (a, b, c) => Network.get('/buyer/shops/list', {
        page_no: b,
        page_size: 10,
        keyword: a,
        sort: c,
        name: a
    }),
    //价格高低
    //低
    searchPrice: (a, b, c) => Network.get('/buyer/goods/search', {page_no: b, page_size: 10, keyword: a, sort: c}),
    //我的中商品
    mineGoods: (a) => Network.get('/buyer/goods/search/recommend', {page_no: a, page_size: 10, category: 491}),
    //验证码
    authCode: () => Network.get('/base/captchas/uGMytogGylPIccvHf2uH/LOGIN', {}),
    //协议
    paction: () => Network.get('/base/pages/REGISTRATION_AGREEMENT/articles', {}),
    //积分商城
    //积分商城的购物
    carts: () => Network.get('/buyer/promotions/exchange/cats', {}),
    //积分商城的积分数据
    integralShop: (a) => Network.get('/buyer/promotions/exchange/goods', {page_no: a, page_size: 10}),
    //拼团
    teamPric: () => Network.get('/buyer/pintuan/goods/skus/uniapp', {page_no: 1, page_size: 10}),
    //团购
    groupbuying: () => Network.get('/buyer/promotions/group-buy/cats', {}),

    //店铺详情
    shopMsg: (a) => Network.get(`/buyer/shops/${a}`, {}),
    //店铺分组信息
    shopType: (a) => Network.get(`/buyer/shops/cats/${a}`, {}),
    //店铺详情中店铺的首页大图
    shopImge: (a) => Network.get(`/buyer/shops/sildes/${a}`, {client_type: 'MOBILE'}),
    //店铺详情中新品推荐
    shopNews: (a) => Network.get(`/buyer/goods/tags/new/goods`, {seller_id: a, mark: 'new', num: 10}),
    //热卖排行
    shopHot: (a) => Network.get(`/buyer/goods/tags/hot/goods`, {seller_id: a, mark: 'hot', num: 10}),
    //推荐商品
    shopRecommend: (a) => Network.get(`/buyer/goods/tags/recommend/goods`, {seller_id: a, mark: 'recommend', num: 10}),
    //店铺详情的全部商品
    shopAlls: (a, b) => Network.get(`/buyer/goods/search`, {page_no: a, page_size: 10, seller_id: b}),
    //商品详情 简介信息
    goodsMsg: (a) => Network.get(`/buyer/goods/${a}/skus`, {}),
    //商品详情 轮播图信息
    goodsSw: (a) => Network.get(`/buyer/goods/${a}`, {}),
    //商品详情未知请求
    goodsOth: (a) => Network.get(`/buyer/promotions/${a}`, {}),
    //购物车总价
    cartPrice: (a) => Network.get('/buyer/trade/carts/all?way=CART', {}, {uuid: uuidv1(), Authorization: a}),
    //购物车
    //t添加
    getTable1H17: (id, num, acid, pt,a) => Network.post('/buyer/trade/carts?sku_id=' + id + '&num=' + num + '&activity_id=' + acid + '&promotion_type=' + pt,{},{uuid: uuidv1(), Authorization: a}),
    //获取
    getTable1H18: (a) => Network.get('buyer/trade/carts/all?way=CART',{},{uuid: uuidv1(), Authorization: a}),
    //减少
    // getTable1H19: (id,num) => Network.post('buyer/trade/carts/sku/'+id,{num:num}),
    //删除
    getTable1H20: (id,a) => Network.delete('buyer/trade/carts/' + id + '/sku',{headers:{uuid: uuidv1(), Authorization: a}}),
    //店铺选择
    getTable1H21: (id) => Network.post('buyer/trade/carts/seller/' + id,{},{uuid: uuidv1(), Authorization: a}),
    //站内消息
    msgS:(a) => Network.get('/buyer/members/member-nocice-logs', {page_no:1,page_size:10,read:0}, {uuid: uuidv1(), Authorization: a}),
}
export default jsp