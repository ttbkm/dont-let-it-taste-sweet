import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios);

// 进行一些全局配置
// 公共路由(网络请求地址)
axios.defaults.baseURL = ' https://api.javamall.com.cn';
// 请求响应超时时间
axios.defaults.timeout = 5000;

// 封装自己的get/post方法
export default {
    get: function (path = '', data = {}, hear = {}) {
        return new Promise(function (resolve, reject) {
            axios.get(path, {
                params: data,
                headers: hear
            })
                .then(function (response) {
                    // console.log(6666, response)
                    resolve(response);
                })
                .catch(function (error) {
                    // console.log(7777, error)
                    reject(error);
                });
        });
    },
    post: function (path = '', data = {}, hear = {}) {
        return new Promise(function (resolve, reject) {
            axios.post(path, data, {headers: hear})
                .then(function (response) {
                    resolve(response);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    },
    delete: function (path = '', data = {}) {
        return new Promise(function (resolve, reject) {
            axios.delete(path, data)
                .then(function (response) {
                    resolve(response);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    }
};