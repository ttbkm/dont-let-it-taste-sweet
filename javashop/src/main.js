import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import serve from './Api/Api'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Vant from 'vant';
import 'vant/lib/index.css';
import {Dialog} from 'vant';

Vue.use(Dialog);
Vue.use(Vant);
Vue.use(VueAxios, axios)
Vue.use(ElementUI);

Vue.prototype.$serve = serve
axios.defaults.withCredentials = true
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
//屏幕自适应
resizeHtml();
window.onresize = resizeHtml;

function resizeHtml() {
    return document.documentElement.style.fontSize = screen.availWidth / 320 * 20 + 'px';
}