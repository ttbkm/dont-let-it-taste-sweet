import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    //首页(路由重定向)
    {path: '', redirect: '/home/homes'},
    {
        path: '/home', component: () => import('../components/Home/Home'), children: [
            //首页商品重定向
            {path: '', redirect: 'homes'},
            //首页商品
            {path: 'homes', component: () => import('../components/Home/Type/Home')},
            //数码家电
            {path: 'sm', component: () => import('../components/Home/Type/Appliance')},
            //进口食品
            {path: 'foods', component: () => import('../components/Home/Type/Foods')},
            //美容化妆
            {path: 'mkp', component: () => import('../components/Home/Type/Makeup')},
            //母婴玩具
            {path: 'child', component: () => import('../components/Home/Type/Child')},
            //厨房用品
            {path: 'ktcn', component: () => import('../components/Home/Type/Kitchen')},
            //钟表箱包
            {path: 'bags', component: () => import('../components/Home/Type/Bags')},
            //营养保健
            {path: 'care', component: () => import('../components/Home/Type/Care')},
            //服装鞋靴
            {path: 'clg', component: () => import('../components/Home/Type/Clothing')},
        ]
    },
    //分类
    {
        path: '/classify', component: () => import('../components/Home/Classify/Classify')
    },
    //我的
    {
        path: '/mine', component: () => import('../components/Home/Mine/Mine')
    },
    //购物车
    {
        path: '/cart', component: () => import('../components/Home/Cart/Cart')
    },
    //主页搜索
    {
        path: '/homeSearch', component: () => import('../components/Home/Search/Homesearch')
    },
    //搜索的商品
    {
        path: '/schgs', component: () => import('../components/Goods/SearchGoods')
    },
    //注册
    {
        path: '/login', component: () => import('../components/Login/Login')
    },
    //登录
    {
        path: '/enter', component: () => import('../components/Login/Enter')
    },
    //重置密码
    {
        path: '/res', component: () => import('../components/Login/Resetpassword')
    },
    //商品详情
    {
        path: '/goods', component: () => import('../components/Goods/Detailed')
    },
    //积分商城
    {
        path: '/int', component: () => import('../components/Goods/Integralshop')
    },
    //拼团
    {
        path: '/team', component: () => import('../components/Goods/Team')
    },
    //团购
    {
        path: '/group', component: () => import('../components/Goods/Groupbuying')
    },
    //领优惠券
    {
        path: '/sale', component: () => import('../components/Goods/Sale')
    },
    //店铺详情
    {
        path: '/shop', component: () => import('../components/Shops/Shops'),children:[
            {path: '', redirect:'home'},
            {path: 'home', component: () => import('../components/Shops/Homepage/Homepage')},
            {path: 'all', component: () => import('../components/Shops/Allgoods/Allgoods')},
            {path: 'ser', component: () => import('../components/Shops/Service/service')},
            {path: 'typ', component: () => import('../components/Shops/Shopstype/Shopstype')},
        ]
    },
]

const router = new VueRouter({
    routes
})
// router.beforeEach((to, from, next) => {
//     // to 将要访问的路径
//     // from 代表从哪一个路径跳转而来
//     // next 是一个函数，表示放行
//     // next() 放行 next('/login') 跳转
//     if (to.path == '/home') {
//         return next();
//     }
//     //获取token
//     const tokenStr = window.sessionStorage.getItem('token')
//     //没有token，强制跳转登录页
//     if (!tokenStr) {
//         return next('/login')
//     }
//     // 放行
//     next()
// });

// router.beforeEach((to, from, next) => {
//     console.log(store.state.token)
// // to: Route: 即将要进入的目标 路由对象
// // from: Route: 当前导航正要离开的路由
// // next: Function: 一定要调用该方法来 resolve 这个钩子。执行效果依赖 next 方法的调用参数。
//     const route = ['index', 'list'];
//     let isLogin = store.state.token; // 是否登录
// // 未登录状态；当路由到route指定页时，跳转至login
//     if (route.indexOf(to.name) >= 0) {
//         if (isLogin == null) {
//             router.push({ path: '/login', });
//         }
//     }
// // 已登录状态；当路由到login时，跳转至home
//     console.log(to.name)
//     localStorage.setItem('routerName', to.name)
//     if (to.name === 'login') {
//         if (isLogin != null) {
//             router.push({ path: '/HomeMain', });;
//         }
//     }
//     next();
// });
export default router
